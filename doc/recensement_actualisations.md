# Recensement des actualisations des barèmes IPP

Ci-dessous se trouve un tableau recensant pour chaque barème la liste des actualisations réalisées. Chaque fois que vous faites une actualisation d'un barème donné, vous devez renseigner à la ligne correspondant au barème :
- votre nom
- la date de l'actualisation
- la nature de l'actualisation (actualisation d'un dispositif en particulier seulement, actualisation de tout le barème, etc.).
- le cas échéant, la merge request (avec le lien hypertexte) associée à vos changements.

Même lorsque vous n'avez fait aucun changement dans les barèmes lors d'une actualisation du fait que la législation n'avait pas changé, vous devez quand même remplir ce tableau afin que la prochaine personne voulant utiliser ou actualiser le barème sache dans quelle mesure celui-ci est à jour.

Afin d'avoir tout l'historique des actualisations, il ne faut pas supprimer les lignes d'avant présentes pour le barème que vous avez actualisé. Pour cela, voici un exemple de code d'une multiligne en markdown :

```
| Barème            | Date        | Nom         | Nature de l'atualisation | Merge request
| -----------       | --------    | --------    | ------                   | --------
| Prélèvements sociaux  | 2021-01-01 <br>2022-01-01  | blabla <br>blabla | blabla <br>blabla | blabla <br>blabla
```
Donne ceci :
| Barème            | Date        | Nom         | Nature de l'atualisation | Merge request
| -----------       | --------    | --------    | ------                   | --------
| Prélèvements sociaux  | 2021-01-01 <br>2022-01-01  | blabla <br>blabla | blabla <br>blabla | blabla <br>blabla


# Tableau des actualisations des barèmes IPP

| Barème                          | Date        | Nom            | Nature de l'atualisation  | Merge request
| -----------                     | --------    | --------       | ------                    | --------
| Prélèvements sociaux            | 2020-03-26  | Sophie @SophieIPP  | Mise à jour et vérification complète | !180
| Impôt sur le revenu             | 2020-08-31 <br> 2020-02-13  | Gautier, Brice @bfabre <br> Chloé @ChloeLallemand | Vérification complète <br> Vérification complète    | !202 <br> !216
| Taxation du capital             | 2021-01-14  | Brice @bfabre  | Vérification complète     |
| Taxation indirecte              |             |                |                           | 
| Fiscalité des entreprises       |             |                |                           | 
| Prestations sociales            | 2020-08-29 <br> 2021-02-12  | Gautier, Brice @bfabre <br> Brice @bfabre | Vérification complète <br> Ajout aides COVID | !203 <br> !223
| Retraites                       |             |                |                           | 
| Chômage                         | 2021-02-15  | Laura @Lauradelmas | Vérification complète <br> Mise à jour complète (Revalorisation salaire de référence - ASS - AER) | !211 !212 !213
| Marché du travail               | 2021-01-18  | Léa @Lead      | Vérification complète <br> Actualisation SMIC et GIPA   | !209
| Tarifs réglementés de l'énergie |             |                |                           | 
