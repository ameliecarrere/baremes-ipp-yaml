# Mise à jour d'un paramètre législatif

## Vérifications à effectuer par le contributeur

- [ ] Renseigner la date de parution au JO
- [ ] Renseigner la référence législative
- [ ] Vérifier que les pipelines passent bien. En cas d'échec, et si le message d'échec est peu clair, s'assurer que les textes (notes, documentation...) incluant des `:` sont mis entre guillemets. ([Voir documentation, partie "My pipeline has failed"](https://framagit.org/french-tax-and-benefit-tables/baremes-ipp-yaml/-/blob/modif-template/doc/guide_edition.md))
- [ ] Ne pas oublier de vérifier que le site web est fonctionnel en cliquant [ici](https://nom_de_la_branche--baremes-ipp.netlify.com/) et en incluant le nom de votre branche dans l'URL s'affichant sur le navigateur.


## Vérifications à effectuer par le relecteur

- [ ] Vérifier la date de parution au JO
- [ ] Vérifier la référence législative
- [ ] Ne pas oublier de vérifier que le site web est fonctionnel en cliquant [ici](https://nom_de_la_branche--baremes-ipp.netlify.com/) et en incluant le nom de votre branche dans l'URL s'affichant sur le navigateur.
