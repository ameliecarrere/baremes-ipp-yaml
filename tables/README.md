# Configuration des tables de paramètres

Les fichiers situés dans ce répertoire définissent la structure de la version Web des paramètres IPP.

Chaque fichier représente une section des paramètres sur https://www.ipp.eu/

Voir la [documentation du format des fichiers](https://framagit.org/french-tax-and-benefit-tables/baremes-ipp-views/blob/master/config-doc.md).
